# Car Management Dashboard
aplikasi API untuk Car Management

-blm lengkap
-revisi menyusul

## Akun superadmin
username : vicase312@gmail.com
password : password

## How to run
guide ...

```bash
how to run
```

## Endpoints
list endpoints ...

## Directory Structure

```
.
├── config
│   └── config.json
├── controllers
├── migrations
├── models
│   └── index.js
├── public
│   ├── css
│   ├── fonts
│   ├── img
│   ├── js
│   └── uploads
├── seeders
├── views
│   ├── templates
│   │   ├── footer.ejs
│   │   └── header.ejs
│   ├── form.ejs
│   └── list.ejs
├── .gitignore
├── README.md
├── index.js
├── package.json
└── yarn.lock
```

## ERD
![Entity Relationship Diagram](public/img/erd.png)
