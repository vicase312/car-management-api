/*
step 2
selanjutnya, coba definisikan express di file index.js yang ada di root project. listen express nya di port 8000 aja, kalau mau port lain boleh asalkan diatas 8000


step ke 3
init model2 yang di perlukan untuk bikin table di database. caranya ketik ini di terminal dimana folder tadi berada:

npm run sequelize-cli model:generate --name User --attributes=username:string,password:string --underscored
*/

const express = require("express")
const swaggerUI = require("swagger-ui-express")
const jwt = require("jsonwebtoken")
const app = express()
const { PORT = 8080 } = process.env

app.use(express.json())


// https://www.npmjs.com/package/swagger-ui-express
const options = {
    swaggerOptions: {
        url: "/api-docs"
    }
}
app.use("/docs", swaggerUI.serve, swaggerUI.setup(null, options))
app.get("/api-docs", (req, res) => {
    res.sendFile(__dirname + "/swagger.yaml")
})
const authenticated = (req, res, next) => {
    try {
        let header = req.headers.authorization.split("Bearer ")[1]
        console.log(header)
        let user = jwt.verify(header, 's3cr3t')
        if (user.username) {
            // user.role
            // user.address
            // user.id
            req.user = user
            next()
            return
        }
    } catch (err) {
        console.log(err.message)
    }

    res.status(401).json({message: "invalid token"})
}

app.get("/api/v1/profile", authenticated, (req, res) => {
    res.status(200).json(req.user)
})

app.post("/api/v1/login", (req, res) => {
    if (req.body.username === "vicase312@gmail.com" && 
        req.body.password === "password") {
            let user = {
                id: 1,
                username: req.body.username,
                role: "SUPERADMIN",
                fullname: "muhammad ilham akbar",
                phone_number: "6286767676767",
                address: "yogyakarta",
            }

            let token = jwt.sign(user, 's3cr3t')

            res.status(200).json({
                token: token
            })
            return
        }

    res.status(401).json({
        message: "invalid email or password"
    })
})


app.listen(PORT, () => {
    console.log(`running on port ${PORT}`)
})
